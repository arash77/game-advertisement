from django.contrib import admin
from django.urls import reverse

from advertisement.models import GamePackage, Advertisement, Banner


class GamePackageAdmin(admin.ModelAdmin):
    list_display = ['name']


class BannerInLine(admin.TabularInline):
    model = Banner


class AdvertisementAdmin(admin.ModelAdmin):
    list_filter = ['game']
    inlines = [BannerInLine]

    def get_list_display(self, request):
        def direct_link(obj):
            return request.build_absolute_uri(
                reverse("banner_pk", args=[obj.id])
            )
        return ['game', 'version', 'placeholder', direct_link, 'modified_time']


class BannerAdmin(admin.ModelAdmin):
    list_display = ['version_name', 'click_url', 'advertisement', 'modified_time']


admin.site.register(GamePackage, GamePackageAdmin)
admin.site.register(Advertisement, AdvertisementAdmin)
admin.site.register(Banner, BannerAdmin)
