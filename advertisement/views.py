from rest_framework.generics import RetrieveAPIView
from django.shortcuts import get_object_or_404

from advertisement.models import Advertisement, Banner
from advertisement.serializers import BannerSerializer


class BannerPkParameterAPIView(RetrieveAPIView):
    queryset = Banner.objects.all()
    serializer_class = BannerSerializer

    def get_object(self):
        queryset = self.get_queryset()                  # Get the base queryset
        queryset = self.filter_queryset(queryset)       # Apply any filter backends
        filter = {'advertisement__id': self.kwargs['pk'], 'version_name': self.request.GET.get('version_name', None)}
        obj = get_object_or_404(queryset, **filter)     # Lookup the object
        self.check_object_permissions(self.request, obj)
        return obj


class BannerAllQueryAPIView(RetrieveAPIView):
    queryset = Banner.objects.all()
    serializer_class = BannerSerializer

    def get_object(self):
        queryset = self.get_queryset()                  # Get the base queryset
        queryset = self.filter_queryset(queryset)       # Apply any filter backends
        filter = {
            'version_name': self.request.GET.get('version_name', None),
            # 'advertisement__placeholder': self.request.GET.get('placeholder', None),
            'advertisement__game__name': self.request.GET.get('package_name', None)
        }
        obj = get_object_or_404(queryset, **filter)     # Lookup the object
        self.check_object_permissions(self.request, obj)
        return obj


class BannerVersionNameParamAPIView(RetrieveAPIView):
    queryset = Banner.objects.all()
    serializer_class = BannerSerializer

    def get_object(self):
        queryset = self.get_queryset()  # Get the base queryset
        queryset = self.filter_queryset(queryset)  # Apply any filter backends
        filter = {
            'version_name': self.kwargs.get('version_name', None),
            # 'advertisement__placeholder': self.request.GET.get('placeholder', None),
            'advertisement__game__name': self.request.GET.get('package_name', None)
        }
        obj = get_object_or_404(queryset, **filter)  # Lookup the object
        self.check_object_permissions(self.request, obj)
        return obj