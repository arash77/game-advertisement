from django.db import models
from django.utils.translation import ugettext_lazy as _


class GamePackage(models.Model):
    name = models.CharField(verbose_name=_("name"), max_length=64)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Advertisement(models.Model):
    game = models.ForeignKey(
        GamePackage, verbose_name=_("game"), related_name='ads'
    )
    placeholder = models.CharField(verbose_name=_("placeholder"), max_length=32)
    version = models.CharField(verbose_name=_("version"), max_length=32)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}({}):{}'.format(self.game.name, self.version, self.placeholder)


class Banner(models.Model):
    version_name = models.CharField(
        verbose_name=_("version_name"), max_length=32, blank=True
    )
    image = models.ImageField(
        upload_to='images/', verbose_name='image', null=True, blank=True
    )
    click_url = models.URLField(verbose_name=_("Click url"), blank=True)

    advertisement = models.ForeignKey(
        Advertisement, verbose_name=_("ad"), related_name='banners'
    )

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}({}):{}'.format(self.advertisement, self.version_name, self.advertisement.placeholder)
