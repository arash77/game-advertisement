from django.conf.urls import url

from advertisement.views import BannerPkParameterAPIView, BannerAllQueryAPIView, BannerVersionNameParamAPIView

urlpatterns = [
    url(r'^(?P<pk>[0-9]+)/$', BannerPkParameterAPIView.as_view(), name='banner_pk'),
    url(r'^$', BannerAllQueryAPIView.as_view(), name='banner_query'),
    url(r'^(?P<version_name>[\w\-]+)/$', BannerVersionNameParamAPIView.as_view(), name='banner_version_name'),
]
