from rest_framework import serializers

from advertisement.models import Advertisement, Banner


class AdvertisementSerializer(serializers.ModelSerializer):
    game = serializers.StringRelatedField()

    class Meta:
        model = Advertisement
        fields = ('game', 'version', 'placeholder')


class BannerSerializer(serializers.ModelSerializer):
    advertisement = AdvertisementSerializer()

    class Meta:
        model = Banner
        fields = ('version_name', 'image', 'click_url', 'advertisement')
